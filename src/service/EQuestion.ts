export enum EStarterValue {
    VANILLA = 'vanilla',
    VUE = 'vue',
    REACT = 'react'
}

export enum ECamera {
    CUBE = 'CubeCamera',
    ORTHOGRAPHIC = 'OrthographicCamera',
    PERSPECTIVE = 'PerspectiveCamera',
    STEREO = 'StereoCamera'
}