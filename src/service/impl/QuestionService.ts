import { prompt } from 'inquirer';
import { IAnswers, IChoice } from '../IQuestion';
import { ECamera, EStarterValue } from '../EQuestion';


export const appNameQuestion = async (): Promise<IAnswers> => {
    return prompt([{
        name: 'appName',
        type: 'input',
        message: 'What is the name of your application ?: '
    }])
};

export const starterQuestion = async (): Promise<IAnswers> => {
    const choiceList: IChoice[] = [
        { choice: 'Vanilla', value: EStarterValue.VANILLA },
        { choice: 'Vue', value: EStarterValue.VUE },
        { choice: 'React', value: EStarterValue.REACT }
    ];

    return prompt([{
        name: 'starter',
        type: 'list',
        message: 'Which starter do you want ?',
        choices: choiceList
    }])
};

export const cameraQuestion = async (): Promise<IAnswers> => {
    const choiceList: IChoice[] = [
        { choice: 'Perspective camera', value: ECamera.PERSPECTIVE },
        { choice: 'Orthographic camera', value: ECamera.ORTHOGRAPHIC },
        { choice: 'Cube camera', value: ECamera.CUBE },
        { choice: 'Stereo camera', value: ECamera.STEREO }
    ];

    return prompt([{
        name: 'camera',
        type: 'list',
        message: 'Which camera do you want to use ?',
        choices: choiceList
    }])
};