import { ECamera, EStarterValue } from './EQuestion';

export interface IAnswers {
    appName: string;
    starter: EStarterValue;
    camera: ECamera;
}

export interface IChoice {
    choice: string;
    value:
        EStarterValue |
        ECamera
}

