import * as fse from "fs-extra";

export class CameraTemplate {

    private imports!: string;
    private methods!: string;
    private fileContent!: string;

    constructor() {
        this.setImports();
        this.setMethods();
        this.fileContent = this.setContent();
    }

    public createFile() {
        const filepath: string = process.cwd() + `\\Camera.js`
        fse.writeFile(filepath, this.fileContent, (error: Error) => {
            if (error) console.error(`File error [CameraTemplate]: ${error.message}`);
            console.info('File [CameraTemplate] created');
        })
    }

    private setImports(): void {
        this.imports = `
import { PerspectiveCamera } from 'three'`;
    }

    private setMethods(): void {
        this.methods = `
    setInstance() {
        this.instance = new PerspectiveCamera(35, this.sizes.width / this.sizes.height, 0.1, 100)
        this.instance.position.set(6, 4, 14)
        this.scene.add(this.instance)
    }`;
    }

    private setContent(): string {
        return `import Experience from './Experience.js'
        import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
        ${this.imports}

export default class Camera {
    constructor() {
        this.experience = new Experience()
        this.sizes = this.experience.sizes
        this.scene = this.experience.scene
        this.canvas = this.experience.canvas

        this.setInstance()
        this.setControls()
    }

    ${this.methods}

    setControls() {
        this.controls = new OrbitControls(this.instance, this.canvas)
        this.controls.enableDamping = true
    }

    resize() {
        this.instance.aspect = this.sizes.width / this.sizes.height
        this.instance.updateProjectionMatrix()
    }

    update() {
        this.controls.update()
    }
}`;
    }
}