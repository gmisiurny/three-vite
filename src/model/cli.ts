#! /usr/bin/env node

import {
    cameraQuestion,
    appNameQuestion,
    starterQuestion
} from '../service/impl/QuestionService';
import colors from 'ansi-colors';
import shell from 'shelljs';
import figlet from 'figlet';
import process from 'process';
import gradient from 'gradient-string';
import {IAnswers} from '../service/IQuestion';
import progress from 'cli-progress';

export class Cli {

    private title = 'cli three';
    private description = `
        THREE-CLI is a command line interface 
        wich provides three.js template over 
        "vanilla", Vue or React application 
        running with Vite        
        `;

    public execute = async () => {
        this.title = gradient.cristal.multiline(
            figlet.textSync(this.title.toUpperCase(),
                {
                    font: 'DOS Rebel',
                    horizontalLayout: 'full',
                    verticalLayout: 'default',
                    width: 120,
                    whitespaceBreak: true
                }));

        this.description = gradient.cristal.multiline(this.description);
        console.info(this.title);
        console.info(this.description);

        const {appName}: IAnswers = await appNameQuestion();
        // await this.Example5();
        await this.starterAction(appName);
        await this.cameraAction();
    }

    private starterAction = async (appName: string) => {
        const {starter}: IAnswers = await starterQuestion();
        // init vite config
        process.env.PROJECT_NAME = appName
        process.env.PROJECT_STARTER = starter;

        shell.exec('vite-config');
    }

    private cameraAction = async () => {
        const {camera}: IAnswers = await cameraQuestion();
        process.env.CAMERA = camera;
        // generate camera file;
    }

    // private Example5 = async () => {
    //     console.log('');
    //     // create new progress bar
    //     const b1 = new progress.Bar({
    //         format: colors.cyan('[{bar}]') + ' {percentage}% || {value}/{total} Chunks || Speed: {speed}',
    //         barCompleteChar: '\u2588',
    //         barIncompleteChar: '\u2591',
    //         hideCursor: true,
    //     });
    //
    //     // initialize the bar -  defining payload token "speed" with the default value "N/A"
    //     b1.start(200, 0, {
    //         speed: "N/A"
    //     });
    //
    //     // the bar value - will be linear incremented
    //     let value = 0;
    //     const speedData: number[] = [];
    //
    //     const timer = setInterval(() => {
    //         value++;
    //         speedData.push(Math.random() * 2 + 5);
    //         const currentSpeedData = speedData.splice(-10);
    //         b1.update(value, {
    //             speed: (currentSpeedData.reduce((a, b) => {
    //                 return a + b;
    //             }, 0) / currentSpeedData.length).toFixed(2) + "Mb/s"
    //         });
    //
    //         if (value >= b1.getTotal()) {
    //             clearInterval(timer);
    //             b1.stop();
    //         }
    //     }, 20);
    //     console.log('');
    // }
}