#! /usr/bin/env node

import shell from 'shelljs';
import process from 'process';

const appName: string = process.env.PROJECT_NAME!.toLowerCase();
const starter = process.env.PROJECT_STARTER;

shell.config.silent = true;
shell.exec(`npm create vite@latest ${ appName } -- --template ${ starter }`);
shell.config.silent = false;
shell.cd(appName);
shell.exec('npm install');
shell.exec('npm install three');
shell.exec('npm install lil-gui');



