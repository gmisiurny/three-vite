import {EStarterValue} from "../src/service/EQuestion";
import shell from "shelljs";
import {CameraTemplate} from "../src/model/CameraTemplate";
import process from "process";

const cloneUrl = 'https://github.com/gmisiurny/three-experience.git';
const starter = process.env.PROJECT_STARTER;
const id: string = (starter === EStarterValue.REACT) ? '<div id="root"></div>' : '<div id="app"></div>'
const canvas = `${ id }\n    <canvas class="webgl"></canvas>`;
const cameraTemplate = new CameraTemplate();

switch (starter) {
    case EStarterValue.VUE:
        shell.sed('-i', id, canvas, 'index.html');
        shell.cd('src');
        shell.exec(`git clone ${ cloneUrl }`);
        shell.cd('three-experience');
        shell.exec('rm -rf .git/');
        shell.cd('..');
        shell.exec('echo const experience = new Experience(document.querySelector("canvas.webgl")) >> main.js');
        shell.sed(
            '-i',
            'import App from \'./App.vue\'',
            'import App from \'./App.vue\'\nimport Experience from \'./three-experience/Experience.js\'',
            'main.js'
        );
        break;

    case EStarterValue.REACT:
        shell.sed('-i', id, canvas, 'index.html');
        shell.cd('src');
        shell.exec(`git clone ${cloneUrl}`);
        shell.cd('three-experience');
        shell.exec('rm -rf .git/');
        shell.cd('..');
        shell.sed(
            '-i',
            'import \'./App.css\'',
            'import \'./App.css\'\nimport Experience from \'./three-experience/Experience.js\'',
            'App.jsx'
        );
        shell.sed(
            '-i',
            'export default App',
            'const experience = new Experience(document.querySelector("canvas.webgl"))\nexport default App',
            'App.jsx'
        );
        break;

    default:
    case EStarterValue.VANILLA:
        shell.exec(`git clone ${cloneUrl}`);
        shell.exec('rm -rf .git/');
        shell.sed('-i', id, canvas, 'index.html');
        shell.sed(
            '-i',
            'import \'./style.css\'',
            'import \'./style.css\'\nimport Experience from \'./three-experience/Experience.js\'',
            'main.js'
        );
        shell.exec('echo const experience = new Experience(document.querySelector("canvas.webgl")) >> main.js');
        shell.cd('three-experience');
        cameraTemplate.createFile();
        break;
}
